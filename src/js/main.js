'use strict';

$(function() {

    // SVG IE11 support
    svg4everybody();

    $('.raty').raty({
        number: 5,
        starHalf      : 'star-half',
        starOff       : 'star-off',
        starOn        : 'star-on',
        cancelOff     : 'cancel-of',
        cancelOn      : 'cancel-on',
        score: function() {
            return $(this).attr('data-score');
        },
        readOnly: function() {
            return $(this).attr('data-readOnly');
        },
    });

    $('.nav-toggle').on("click", function (e) {
        e.preventDefault();
        $('.page').toggleClass('nav-open');
    });

    $('.countries-toggle').on("click", function (e) {
        e.preventDefault();
        $('.countries').toggleClass('open');
    });

    let vendors = new Swiper('.vendors__slider', {
        slidesPerView: 'auto',
        direction: 'horizontal',
        freeMode: true,
        scrollbar: {
          el: '.vendors__scrollbar',
        },
        mousewheel: true,
    });

    let offer = new Swiper('.active .offer__slider', {
        loop: true,
        spaceBetween: 16,
        slidesPerView: 1,
        navigation: {
            nextEl: '.offer-next',
            prevEl: '.offer-prev',
        },
        breakpoints: {
            567: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            768: {
              slidesPerView: 3,
              spaceBetween: 20,
            },
            1024: {
              slidesPerView: 4,
              spaceBetween: 15,
            }
        }
    });

    $(".offer__nav--item").on("click", function (e)  {
        e.preventDefault();
        let tab = $(this).attr("data-tab");
        offer.destroy();

        $('.offer__nav--item').removeClass('active')
        $(this).toggleClass('active');

        $('.offer__tab').removeClass('active');
        $(tab).addClass('active');

        offer = new Swiper('.active .offer__slider', {
            loop: true,
            spaceBetween: 16,
            slidesPerView: 1,
            navigation: {
                nextEl: '.offer-next',
                prevEl: '.offer-prev',
            },
            breakpoints: {
                567: {
                    slidesPerView: 2,
                    spaceBetween: 20,
                },
                768: {
                  slidesPerView: 3,
                  spaceBetween: 20,
                },
                1024: {
                  slidesPerView: 4,
                  spaceBetween: 15,
                }
            }
        });
    });

    let comments = new Swiper('.comments__slider', {
        loop: true,
        spaceBetween: 10,
        slidesPerView: 1,
        navigation: {
            nextEl: '.comments-next',
            prevEl: '.comments-prev',
        },
        breakpoints: {
            768: {
              slidesPerView: 1,
              spaceBetween: 20,
            },
            1024: {
              slidesPerView: 1,
              spaceBetween: 20,
            }
        }
    });

    let articles = new Swiper('.articles__slider', {
        loop: true,
        spaceBetween: 10,
        slidesPerView: 1,
        breakpoints: {
            567: {
              slidesPerView: 2,
              spaceBetween: 10,
            },
            768: {
              slidesPerView: 2,
              spaceBetween: 20,
            }
        }
    });

    let primary = new Swiper('.primary__slider', {
        loop: true,
        spaceBetween: 10,
        slidesPerView: 1,
        navigation: {
            nextEl: '.primary-next',
            prevEl: '.primary-prev',
        },
        breakpoints: {
            567: {
              slidesPerView: 1,
              spaceBetween: 10,
            },
            768: {
              slidesPerView: 1,
              spaceBetween: 30,
            }
        }
    });

    // History
    let historyPrimary = new Swiper('.history-slider-primary', {
        loop: true,
        spaceBetween: 10,
        slidesPerView: 1,
        navigation: {
            nextEl: '.history-next',
            prevEl: '.history-prev',
        },
    });

    let historyPrev = new Swiper('.history-slider-prev', {
        loop: true,
        spaceBetween: 10,
        slidesPerView: 1,
        shortSwipes: false,
        longSwipes: false,
        simulateTouch: false,
    });

    let historyNext = new Swiper('.history-slider-next', {
        loop: true,
        spaceBetween: 10,
        slidesPerView: 1,
        shortSwipes: false,
        longSwipes: false,
        simulateTouch: false,
    });

    historyPrimary.on('slidePrevTransitionStart', function () {
        historyPrev.slidePrev (300);
        historyNext.slidePrev (300);
    });
    historyPrimary.on('slideNextTransitionStart', function () {
        historyPrev.slideNext (300);
        historyNext.slideNext (300);
    });

    let rest = new Swiper('.rest-slider', {
        loop: true,
        spaceBetween: 20,
        slidesPerView: 'auto',
        navigation: {
            nextEl: '.rest-next',
            prevEl: '.rest-prev',
        },
        breakpoints: {
            768: {
                slidesPerView: 'auto',
                spaceBetween: 40,
            },
            1024: {
                slidesPerView: 6,
                spaceBetween: 24,
            }
        }
    });

    let places = new Swiper('.places-slider', {
        loop: true,
        spaceBetween: 10,
        slidesPerView: 'auto',
        navigation: {
            nextEl: '.places-next',
            prevEl: '.places-prev',
        },
        breakpoints: {
            768: {
                slidesPerView: 'auto',
                spaceBetween: 15,
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 15,
            }
        }
    });

    let hotels = new Swiper('.hotels-slider', {
        loop: true,
        spaceBetween: 16,
        slidesPerView: 1,
        navigation: {
            nextEl: '.hotels-next',
            prevEl: '.hotels-prev',
        },
        breakpoints: {
            567: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 20,
            },
            1024: {
                slidesPerView: 4,
                spaceBetween: 15,
            }
        }
    });

    let programs = new Swiper('.programs-slider', {
        loop: true,
        spaceBetween: 10,
        slidesPerView: 'auto',
        navigation: {
            nextEl: '.programs-next',
            prevEl: '.programs-prev',
        },
        breakpoints: {
            768: {
                spaceBetween: 15,
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 15,
            }
        }
    });

    let info_block = new Swiper('.info-block-slider', {
        loop: false,
        spaceBetween: 20,
        simulateTouch: false,
        slidesPerView: 'auto',
        breakpoints: {
            768: {
                slidesPerView: 'auto',
                spaceBetween: 40,
            },
            1024: {
                slidesPerView: 6,
                spaceBetween: 40,
                simulateTouch: false
            }
        }
    });

    let child_program = new Swiper('.child-program-slider', {
        loop: false,
        simulateTouch: false,
        spaceBetween: 5,
        slidesPerView: 'auto',
        breakpoints: {
            768: {
                slidesPerView: 'auto',
                spaceBetween: 10,
            },
            1024: {
                slidesPerView: 5,
                spaceBetween: 10,
            }
        }
    });

    let child_hotels = new Swiper('.child-hotels-slider', {
        loop: false,
        spaceBetween: 10,
        slidesPerView: 'auto',
        navigation: {
            nextEl: '.child-hotels-next',
            prevEl: '.child-hotels-prev',
        },
        breakpoints: {
            768: {
                slidesPerView: 'auto',
                spaceBetween: 15,
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 15,
            }
        }
    });

    let place = new Swiper('.place-slider', {
        loop: false,
        spaceBetween: 10,
        slidesPerView: 'auto',
        breakpoints: {
            768: {
                slidesPerView: 3,
                spaceBetween: 20,
            }
        }
    });

    let conditions = new Swiper('.conditions-slider', {
        loop: false,
        spaceBetween: 10,
        slidesPerView: 'auto'
    });

    let relax = new Swiper('.relax-slider', {
        loop: false,
        spaceBetween: 10,
        slidesPerView: 1,
        navigation: {
            nextEl: '.relax-next',
            prevEl: '.relax-prev',
        },
        breakpoints: {
            768: {
                spaceBetween: 20,
            }
        }
    });

    let tour = new Swiper('.tour-slider', {
        loop: false,
        spaceBetween: 5,
        slidesPerView: 'auto'
    });

    let attraction = new Swiper('.attraction-slider', {
        loop: false,
        spaceBetween: 10,
        simulateTouch: false,
        slidesPerView: 'auto',
        breakpoints: {
            768: {
                spaceBetween: 20,
                slidesPerView: 2,
            }
        }
    });

    let promo = new Swiper('.promo-slider', {
        loop: true,
        spaceBetween: 30,
        slidesPerView: 1,
        navigation: {
            nextEl: '.promo-next',
            prevEl: '.promo-prev',
        },
    });

    let weekend_services= new Swiper('.weekend-services__slider', {
        loop: false,
        spaceBetween: 20,
        simulateTouch: false,
        slidesPerView: 'auto',
        breakpoints: {
            768: {
                slidesPerView: 5,
                spaceBetween: 25,
            },
            1024: {
                slidesPerView: 5,
                spaceBetween: 75
            }
        }
    });

    let tour_tabs_slider = new Swiper('.tour-tabs-slider', {
        loop: true,
        spaceBetween: 0,
        slidesPerView: 'auto',
        slideVisibleClass: 'swiper-slide-visible',
        breakpoints: {
            1024: {
                slidesPerView: 4,
            }
        }
    });

    $('.tour-tabs-item').on('click', function(e) {
        e.preventDefault();
        let $box = $(this).closest('.tour-tabs');
        let $tab = $($(this).attr('href'));

        $box.find('.tour-tabs-item').removeClass('active');
        $(this).addClass('active');
        $box.find('.tour-tabs-elem').removeClass('active');
        $tab.addClass('active');
    })

    let info = new Swiper('.info-box-slider', {
        loop: true,
        spaceBetween: 10,
        slidesPerView: 'auto',
        navigation: {
            nextEl: '.info-next',
            prevEl: '.info-prev',
        },
        breakpoints: {
            768: {
                slidesPerView: 'auto',
                spaceBetween: 15,
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 15,
            }
        }
    });


    let service_slider = new Swiper('.service-slider', {
        loop: true,
        spaceBetween: 20,
        slidesPerView: 'auto',
        navigation: {
            nextEl: '.service-next',
            prevEl: '.service-prev',
        },
        breakpoints: {
            1024: {
                slidesPerView: 4,
                spaceBetween: 15,
            }
        }
    });
});



